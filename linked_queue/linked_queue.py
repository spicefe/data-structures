# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.

class LinkedQueueNode:

    def __init__(self, value, link=None):
        self.value = value
        self.link = link

class LinkedQueue:

    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail

    # Enqueue: add Note to back of the queue
    def enqueue(self, val):
        # Instantiate new node with value
        n = LinkedQueueNode(val)

        # If queue is empty
        if self.head is None:
            self.head = n
            self.tail = n

        # If queue has members
        else:
            # link for old tail becomes new node
            self.tail.link = n

            # tail points to new tail
            self.tail = n
        # enqueue does not need to return a value
        return None

    # Dequeue: remove from front of the queue and return value

    def dequeue(self):

        # Queue cannot be empty
        assert self.head, "Queue is empty."

        # Temporarily store value for node at head
        _value = self.head.value

        # Reassign head property to node
        # current head's link points to
        self.head = self.head.link

        # If final item removed
        # set tail to None as well
        if self.head is None:
            self.tail = None

        return _value
